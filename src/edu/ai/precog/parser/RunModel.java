package edu.ai.precog.parser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;

import weka.classifiers.Classifier;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffLoader;


import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;
import javax.swing.text.AttributeSet;

import edu.ai.precog.parser.util.ListDialog;

/**
 * Class for diaplaying the GUI
 * 
 */
public class RunModel extends JPanel implements ActionListener
{
	private static final long serialVersionUID = 1L;

	public static final String CHANCES_OF = "chance of ";

	public static final String TEMP_FILE_PATH = "gui/.tempClassify.arff";

	public static final String[] ZONES = {"Allston" , "Dorchester" , "BackBay" , 
		"BeaconHillBayVillageChinatown" , "Charlestown" , "SouthBoston" , "Downtown" , 
		"EastBoston" , "Fenway" , "JamaicaPlain" , "Mattapan" , "MissionHillLMA" ,
		"NorthEndWestEnd" , "Brighton" , "SouthEnd" , "Roslindale" , "Roxbury" , 
		"SouthZone" , "WestRoxbury"};

	public static final String[] CRIME_TYPES = {"stolen property",
		"vandalism",
		"breaking",
		"auto_theft",
		"assault",
		"murder",
		"disorderly people (fights, annoyances)",
	"robbery"};

	public static final String DELIM = ",";
	static protected JFrame frame;
	protected final String exText = "Ex. 'huntington ave'";

	public static final String[] HOUR = {"0","1","2","3","4","5","6","7","8","9",
		"10","11","12","13","14","15","16","17","18","19","20",
		"21","22","23"};

	public static final String[] DAY = {"1","2","3","4","5","6","7","8","9",
		"10","11","12","13","14","15","16","17","18","19","20",
		"21","22","23","24","25","26","27","28","29","30","31"};

	public static final String[] MONTH = {"1","2","3","4","5","6","7","8","9",
		"10","11","12"};

	protected JButton predictBut, streetBut;
	protected JComboBox zoneList, dayList, monthList, hourList;
	protected JTextField streetField, tempField;

	protected java.util.List<String> streets;

	/**
	 * Constructor to initialize and display the required components
	 */
	public RunModel()
	{

		super(new BorderLayout());

		BufferedReader reader = null;
		try 
		{
			reader = new BufferedReader(new FileReader(new File("gui/street_dict.txt")));
			fillStreetListAndAlphabetize(reader);
		} 
		catch (IOException e1) 
		{
			e1.printStackTrace();
		}

		JPanel intro = new JPanel();
		JLabel introLabel = new JLabel("<html><b><h1>AI Final project<br><pre>Precog</pre><br></h1></b><br>" +
				"<b><h3>Enter information to predict crime.</h3></b></html>");
		intro.add(introLabel);
		add(intro,BorderLayout.NORTH);



		JPanel inputs = new JPanel(new GridLayout(0,1,4,4));		
		JPanel labels = new JPanel(new GridLayout(0,1,4,4));


		JLabel zoneLabel = new JLabel("Zone:  ");
		labels.add(zoneLabel);

		zoneList = new JComboBox(ZONES);
		zoneList.addActionListener(this);
		inputs.add(zoneList);

		JLabel streetLabel = new JLabel("Street:  ");
		labels.add(streetLabel);


		streetBut = new JButton("Choose...");
		streetBut.setVerticalTextPosition(AbstractButton.CENTER);
		streetBut.setHorizontalTextPosition(AbstractButton.CENTER);
		streetBut.setMnemonic(KeyEvent.VK_D);
		streetBut.setActionCommand("street");
		streetBut.addActionListener(this);
		inputs.add(streetBut);

		JLabel monthLabel = new JLabel("Month:  ");
		labels.add(monthLabel);

		monthList = new JComboBox(MONTH);
		monthList.addActionListener(this);
		inputs.add(monthList);

		JLabel dayLabel = new JLabel("Day:  ");
		labels.add(dayLabel);

		dayList = new JComboBox(DAY);
		dayList.addActionListener(this);
		inputs.add(dayList);

		JLabel hourLabel = new JLabel("Hour:  ");
		labels.add(hourLabel);

		hourList = new JComboBox(HOUR);
		hourList.addActionListener(this);
		inputs.add(hourList);

		JLabel tempLabel = new JLabel("Temp (f):  ");
		labels.add(tempLabel);

		tempField = new JTextField();
		final int limit = 2;
		tempField.setDocument(new PlainDocument(){
			private static final long serialVersionUID = 1L;

			@Override
			public void insertString(int offs, String str, AttributeSet a)
					throws BadLocationException {
				String pattern = "[^\\d]";  //non numerics

				if(getLength() + str.length() <= limit && !str.matches(pattern))
					super.insertString(offs, str, a);
			}
		});
		
		inputs.add(tempField);

		add(inputs,BorderLayout.EAST);
		add(labels,BorderLayout.WEST);

		predictBut = new JButton("Predict");
		predictBut.setVerticalTextPosition(AbstractButton.CENTER);
		predictBut.setHorizontalTextPosition(AbstractButton.CENTER);
		predictBut.setMnemonic(KeyEvent.VK_D);
		predictBut.setActionCommand("predict");

		predictBut.addActionListener(this);

		JPanel authors = new JPanel(new GridLayout(0,1,4,4));
		JLabel authorLabel = new JLabel("<html><i><h4>Authors:  FL,AJ,VP,PD</h4></i></html>");
		authorLabel.setHorizontalAlignment(0);
		authors.add(predictBut);
		authors.add(authorLabel);
		add(authors,BorderLayout.SOUTH);
	}

	/**
	 * Fills the Street List and sort the contents
	 * @param reader
	 * @throws IOException
	 */
	private void fillStreetListAndAlphabetize(BufferedReader reader) throws IOException
	{
		String[] split = reader.readLine().split(",");
		this.streets = Arrays.asList(split);
		Collections.sort(this.streets);
	}

	public static void main(String[] args) throws ParseException 
	{
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() 
			{
				createAndShowGUI();
			}
		});
	}

	public static double[] classify()  
	{
		// Create attributes to be used with classifiers
		// Test the model
		double[] result = null;
		try 
		{
			// load data
			ArffLoader loader = new ArffLoader();
			loader.setFile(new File(TEMP_FILE_PATH));
			Instances structure = loader.getStructure();
			structure.setClassIndex(structure.numAttributes() - 1);

			//this is the first instance in the testing_set.arff file.
			Instance current = loader.getNextInstance(structure);


			// load classifier from file
			Classifier cls_co = (Classifier) 
					weka.core.SerializationHelper.read("models/47_streets_bayesnet_2ndit.model");

			result = cls_co.distributionForInstance(current);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Create the GUI and show it.  For thread safety,
	 * this method should be invoked from the
	 * event-dispatching thread.
	 */
	private static void createAndShowGUI() {
		//Create and set up the window.
		frame = new JFrame("Non-Domestic Crime Prediction in Boston - PRECOG");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//Create and set up the content pane.
		JComponent newContentPane = new RunModel();
		newContentPane.setOpaque(true); //content panes must be opaque
		frame.setContentPane(newContentPane);

		//Display the window.
		frame.pack();
		frame.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if("predict".equals(e.getActionCommand()))
		{
			//button has been pressed
			String temp = (String)tempField.getText();
			if(temp == null || temp.equals(""))
				JOptionPane.showMessageDialog(frame,
						"Enter a correct Temp!",
						"Error",
						JOptionPane.ERROR_MESSAGE);
			else if(streetBut.getText().contains("Choose..."))
				JOptionPane.showMessageDialog(frame,
						"You forgot to choose a street!",
						"Error",
						JOptionPane.ERROR_MESSAGE);
			else
				startClassification((String)zoneList.getSelectedItem(), (String)hourList.getSelectedItem(), 
						(String)dayList.getSelectedItem(), (String)monthList.getSelectedItem(), 
						tempField.getText(), streetBut.getText());
		}
		else if("street".equals(e.getActionCommand()))
		{
			//show street choose dialog
			streetBut.setText(ListDialog.showDialog(
					frame,
					streetBut,
					"<html><b><h1>NOTE: You can type in first few letters to skip ahead",
					"Choose a Street</b></html>",
					(String[])streets.toArray(),
					streetBut.getText(),
					"st_alphonsus_st    "));
		}
	}

	/**
	 * 
	 * @param zone
	 * @param hour
	 * @param day
	 * @param month
	 * @param temp
	 * @param street
	 */
	private void startClassification(String zone, String hour, String day, 
			String month, String temp, String street)
	{

		BufferedReader reader = null;
		BufferedWriter writer = null;
		//read from header, append our line, write to temp file
		try 
		{
			//read from header, append our line, write to temp file
			reader = new BufferedReader(new FileReader(new File("gui/header.arff")));
			ArrayList<String> headerData = new ArrayList<String>();
			String line = null;
			while ((line = reader.readLine()) != null) 
			{
				headerData.add(line);
			}
			reader.close();

			//headerdata is imported, generate line.. ex:   102,12,40,?,Fenway,huntington_av

			//parse month and date...
			String month_day = month+"/"+day;
			SimpleDateFormat sd = new SimpleDateFormat("MM/dd");
			String dayOfYear = null;

			Date currentDate = sd.parse(month_day);
			GregorianCalendar cal = new GregorianCalendar();
			cal.setTime(currentDate);

			dayOfYear = Integer.toString(cal.get(GregorianCalendar.DAY_OF_YEAR));

			String instance = dayOfYear + DELIM + 
								hour + DELIM + 
								temp + DELIM + 
								"?" + DELIM + 
								zone + DELIM + 
								street;

			//now write the temp file
			writer = new BufferedWriter(new FileWriter(new File(TEMP_FILE_PATH)));
			for(String s : headerData)
			{
				writer.write(s);
				writer.newLine();
			}
			writer.write(instance);
			writer.close();

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		double[] result = classify();

		//

		if(result != null)
		{
			ArrayList<String> outputData = new ArrayList<String>();

			double max = -1;
			int index = -1;

			for(int i = 0; i < result.length; i++)
			{
				double dist = result[i];
				if (dist > max){
					max = dist;
					index = i;
				}

				outputData.add( new DecimalFormat("#.##").format(dist*100).toString() + "% "
						+ CHANCES_OF + CRIME_TYPES[i] + "<br>");
			}

			outputData.add( "<br><br>Most likely crime in area classified as: <h2>" + CRIME_TYPES[index]+ "</h2>");

			String finalResult = "<html>";
			for(String s : outputData)
			{
				finalResult += s;
			}
			finalResult += "</html>";

			JOptionPane.showMessageDialog(frame,
					finalResult,
					"Results:",
					JOptionPane.PLAIN_MESSAGE);
		}
	}
}
