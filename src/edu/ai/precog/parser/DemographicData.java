package edu.ai.precog.parser;

public class DemographicData 
{
	public static final String DELIM = ",";
	
	//    DATA FROM:
	//      http://www.boston.com/yourtown/boston/census_2010/
	
	//   INCOME DATA FROM:
	//     	http://www.point2homes.com/US/Neighborhood/MA/Boston-Demographics.html
	
	
	// demographics go as: POP, WHITE, BLACK, HISPANIC, ASIAN, OTHER, MIX , OCCUPIED_HOUSING, VACANT_HOUSING
	public static final String allston_dem = 
			"27614" + DELIM +      //POP
			".6162" + DELIM +     // WHITE PERC
			".0437" + DELIM +     // BLACK PERC
			".1061" + DELIM +     // HISPANIC PERC
			".1873" + DELIM +     // ASIAN PERC
			".0163" + DELIM +     // OTHER PERC
			".0290" + DELIM +    //MIX PERC
			".9657" + DELIM +    //OCCUPIED HOUSING PERC   
			".0343" + DELIM +    //VACANT HOUSING PERC
			"42971";  			//MEDIAN INCOME  
	
	public static final String backbay_dem = 
			"17070" + DELIM +      //POP
			".7820" + DELIM +     // WHITE PERC
			".0394" + DELIM +     // BLACK PERC
			".0578" + DELIM +     // HISPANIC PERC
			".1013" + DELIM +     // ASIAN PERC
			".0029" + DELIM +     // OTHER PERC
			".0158" + DELIM +    //MIX PERC
			".8682" + DELIM +    //OCCUPIED HOUSING PERC   
			".1318" + DELIM +    //VACANT HOUSING PERC
			"68469";  			//MEDIAN INCOME 
	
	public static final String beaconhill_westend_dem = 
			"17070" + DELIM +      //POP
			".8247" + DELIM +     // WHITE PERC
			".0239" + DELIM +     // BLACK PERC
			".0451" + DELIM +     // HISPANIC PERC
			".0896" + DELIM +     // ASIAN PERC
			".0026" + DELIM +     // OTHER PERC
			".0134" + DELIM +    //MIX PERC
			".9019" + DELIM +    //OCCUPIED HOUSING PERC   
			".0981" + DELIM +    //VACANT HOUSING PERC
			"86254";//86,254 or 78,146		//MEDIAN INCOME 
	
	public static final String brighton_dem = 
			"41953" + DELIM +      //POP
			".7247" + DELIM +     // WHITE PERC
			".0409" + DELIM +     // BLACK PERC
			".0753" + DELIM +     // HISPANIC PERC
			".1284" + DELIM +     // ASIAN PERC
			".0112" + DELIM +     // OTHER PERC
			".0184" + DELIM +    //MIX PERC
			".9558" + DELIM +    //OCCUPIED HOUSING PERC   
			".0442" + DELIM +    //VACANT HOUSING PERC
			"42971";  			//MEDIAN INCOME 
	
	public static final String charlestown_dem = 
			"13754" + DELIM +      //POP
			".8038" + DELIM +     // WHITE PERC
			".0356" + DELIM +     // BLACK PERC
			".0707" + DELIM +     // HISPANIC PERC
			".0774" + DELIM +     // ASIAN PERC
			".0024" + DELIM +     // OTHER PERC
			".0087" + DELIM +    //MIX PERC
			".9289" + DELIM +    //OCCUPIED HOUSING PERC   
			".0711" + DELIM +    //VACANT HOUSING PERC
			"86412";  			//MEDIAN INCOME 
	
	public static final String dorchester_dem = 
			"70436" + DELIM +      //POP
			".3185" + DELIM +     // WHITE PERC
			".3567" + DELIM +     // BLACK PERC
			".1221" + DELIM +     // HISPANIC PERC
			".1163" + DELIM +     // ASIAN PERC
			".0487" + DELIM +     // OTHER PERC
			".0352" + DELIM +    //MIX PERC
			".9155" + DELIM +    //OCCUPIED HOUSING PERC   
			".0845" + DELIM +    //VACANT HOUSING PERC
			"43072";  			//MEDIAN INCOME
	
	public static final String downtown_chinatown_leatherdist_dem = 
			"15226" + DELIM +      //POP
			".5649" + DELIM +     // WHITE PERC
			".0681" + DELIM +     // BLACK PERC
			".0581" + DELIM +     // HISPANIC PERC
			".2906" + DELIM +     // ASIAN PERC
			".0022" + DELIM +     // OTHER PERC
			".0145" + DELIM +    //MIX PERC
			".8377" + DELIM +    //OCCUPIED HOUSING PERC   
			".1623" + DELIM +    //VACANT HOUSING PERC
			"88685";//88,685 or 69,304  			//MEDIAN INCOME 
	
	public static final String east_boston_dem = 
			"31483" + DELIM +      //POP
			".4135" + DELIM +     // WHITE PERC
			".0302" + DELIM +     // BLACK PERC
			".4885" + DELIM +     // HISPANIC PERC
			".0354" + DELIM +     // ASIAN PERC
			".0172" + DELIM +     // OTHER PERC
			".0137" + DELIM +    //MIX PERC
			".9241" + DELIM +    //OCCUPIED HOUSING PERC   
			".0759" + DELIM +    //VACANT HOUSING PERC
			"44611";  			//MEDIAN INCOME 
	
	public static final String fenway_kenmore_dem = 
			"33117" + DELIM +      //POP
			".6552" + DELIM +     // WHITE PERC
			".0476" + DELIM +     // BLACK PERC
			".0791" + DELIM +     // HISPANIC PERC
			".1855" + DELIM +     // ASIAN PERC
			".0033" + DELIM +     // OTHER PERC
			".0276" + DELIM +    //MIX PERC
			".9556" + DELIM +    //OCCUPIED HOUSING PERC   
			".0444" + DELIM +    //VACANT HOUSING PERC
			"38792";  			//MEDIAN INCOME 
	
	public static final String hyde_park_dem = 
			"23451" + DELIM +      //POP
			".3217" + DELIM +     // WHITE PERC
			".4615" + DELIM +     // BLACK PERC
			".1743" + DELIM +     // HISPANIC PERC
			".0163" + DELIM +     // ASIAN PERC
			".0068" + DELIM +     // OTHER PERC
			".0170" + DELIM +    //MIX PERC
			".9376" + DELIM +    //OCCUPIED HOUSING PERC   
			".0624" + DELIM +    //VACANT HOUSING PERC
			"49768";  			//MEDIAN INCOME  
	
	public static final String jamaicanplain_dem = 
			"31141" + DELIM +      //POP
			".5800" + DELIM +     // WHITE PERC
			".1272" + DELIM +     // BLACK PERC
			".2204" + DELIM +     // HISPANIC PERC
			".0465" + DELIM +     // ASIAN PERC
			".0048" + DELIM +     // OTHER PERC
			".0189" + DELIM +    //MIX PERC
			".9468" + DELIM +    //OCCUPIED HOUSING PERC   
			".0532" + DELIM +    //VACANT HOUSING PERC
			"68980";  			//MEDIAN INCOME 
	
	public static final String mattapan_dem = 
			"26503" + DELIM +      //POP
			".0307" + DELIM +     // WHITE PERC
			".7703" + DELIM +     // BLACK PERC
			".1554" + DELIM +     // HISPANIC PERC
			".0070" + DELIM +     // ASIAN PERC
			".0001" + DELIM +     // OTHER PERC
			".0231" + DELIM +    //MIX PERC
			".9106" + DELIM +    //OCCUPIED HOUSING PERC   
			".0894" + DELIM +    //VACANT HOUSING PERC
			"45924";  			//MEDIAN INCOME 
	
	public static final String north_end_dem = 
			"9667" + DELIM +      //POP
			".9133" + DELIM +     // WHITE PERC
			".0109" + DELIM +     // BLACK PERC
			".0349" + DELIM +     // HISPANIC PERC
			".0281" + DELIM +     // ASIAN PERC
			".0024" + DELIM +     // OTHER PERC
			".0087" + DELIM +    //MIX PERC
			".9153" + DELIM +    //OCCUPIED HOUSING PERC   
			".0847" + DELIM +    //VACANT HOUSING PERC
			"94543";  			//MEDIAN INCOME 
	
	public static final String rosindale_dem = 
			"22270" + DELIM +      //POP
			".5145" + DELIM +     // WHITE PERC
			".2061" + DELIM +     // BLACK PERC
			".2291" + DELIM +     // HISPANIC PERC
			".0278" + DELIM +     // ASIAN PERC
			".0059" + DELIM +     // OTHER PERC
			".0144" + DELIM +    //MIX PERC
			".9344" + DELIM +    //OCCUPIED HOUSING PERC   
			".0656" + DELIM +    //VACANT HOUSING PERC
			"60948";  			//MEDIAN INCOME
	
	public static final String roxbury_missionhill_longwood_dem = 
			"60028" + DELIM +      //POP
			".2556" + DELIM +     // WHITE PERC
			".4049" + DELIM +     // BLACK PERC
			".2182" + DELIM +     // HISPANIC PERC
			".0652" + DELIM +     // ASIAN PERC
			".0236" + DELIM +     // OTHER PERC
			".0283" + DELIM +    //MIX PERC
			".9302" + DELIM +    //OCCUPIED HOUSING PERC   
			".0698" + DELIM +    //VACANT HOUSING PERC
			"41396"; //41,396 or 33,655		//MEDIAN INCOME 
	
	public static final String south_boston_dem = 
			"30675" + DELIM +   //POP
			".8019" + DELIM +   // WHITE PERC
			".0555" + DELIM +   // BLACK PERC
			".0854" + DELIM +   // HISPANIC PERC
			".0439" + DELIM +   // ASIAN PERC
			".0030" + DELIM +   // OTHER PERC
			".0087" + DELIM +   //MIX PERC
			".9203" + DELIM +   //OCCUPIED HOUSING PERC   
			".0797" + DELIM +   //VACANT HOUSING PERC
			"68223";  			//MEDIAN INCOME 
	
	public static final String southend_bayvillage_dem = 
			"22724" + DELIM +    //POP
			".5918" + DELIM +    // WHITE PERC
			".1097" + DELIM +    // BLACK PERC
			".1107" + DELIM +    // HISPANIC PERC
			".1659" + DELIM +    // ASIAN PERC
			".0039" + DELIM +    // OTHER PERC
			".0158" + DELIM +    //MIX PERC
			".9408" + DELIM +    //OCCUPIED HOUSING PERC   
			".0592" + DELIM +    //VACANT HOUSING PERC
			"57872"; //57,872 or 94,169 			//MEDIAN INCOME 
	
	public static final String west_roxbury_dem = 
			"24120" + DELIM +   //POP
			".7694" + DELIM +   // WHITE PERC
			".0823" + DELIM +   // BLACK PERC
			".0728" + DELIM +   // HISPANIC PERC
			".0614" + DELIM +   // ASIAN PERC
			".0033" + DELIM +   // OTHER PERC
			".0095" + DELIM +   //MIX PERC
			".9567" + DELIM +   //OCCUPIED HOUSING PERC   
			".0433" + DELIM +   //VACANT HOUSING PERC
			"77301";  			//MEDIAN INCOME 
	}
