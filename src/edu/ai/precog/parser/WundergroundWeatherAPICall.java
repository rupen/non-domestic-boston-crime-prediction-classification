package edu.ai.precog.parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class WundergroundWeatherAPICall {

	public static String DELIM = ",";
	public static String API_KEY = "6e6a955b7d5ff9bd";
	public static String API_CALL = "http://api.wunderground.com/api/6e6a955b7d5ff9bd/history_%s/q/MA/Boston.json";
	
	
	
	
	//NOT WORKING ANYMORE (OLD API)
	//TO USE:
//	//get and split the weather data
//	String[] weatherData = getWeatherDataFromData(fromDate, Integer.parseInt(hourOfDay_output)).split(DELIM);
//	String temperature_output = weatherData[0];
//	String humidity_output = weatherData[1];
//	String rain_output = weatherData[2];
//	String snow_output = weatherData[3];
//	
	
	// returns temp,humid,snow,rain
		public static String getWeatherDataFromData(String fromDate, int hourOfDay)
		{
			String result = null;
			
			
			//make api date in format   yyyymmdd
			String[] apiDateSplit = fromDate.split(" ")[0].split("/");
			String api_month = apiDateSplit[0];
			if(api_month.length() == 1)
				api_month = "0" + api_month;
			
			String api_day = apiDateSplit[1];
			if(api_day.length() == 1)
				api_day = "0" + api_day;
			
			String api_year = apiDateSplit[2];
			fromDate = api_year+api_month+api_day;
			
			
			URL url;
			try 
			{
				url = new URL(String.format(API_CALL, fromDate));
				
				URLConnection connection = url.openConnection();
				//connection.addRequestProperty("Referer", );

				String line;
				StringBuilder builder = new StringBuilder();
				BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				while((line = reader.readLine()) != null) {
				 builder.append(line);
				}

				JSONObject json = (JSONObject)new JSONParser().parse(builder.toString());

				
				JSONObject history = (JSONObject) json.get("history");
				JSONArray observations = (JSONArray) history.get("observations");
				JSONObject hourObs = (JSONObject)observations.get(hourOfDay);
				
				String temp = (String)hourObs.get("tempi");
				String humidity = (String)hourObs.get("hum");
				String rain = (String)hourObs.get("rain");
				String snow = (String)hourObs.get("snow");
				
				result = temp + DELIM + humidity + DELIM + rain + DELIM + snow; 
				
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return result;
		}
}
