package edu.ai.precog.parser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;


public class CSVGen 
{
	public static String ORIGINAL_CSV_PATH 	= "res/crime_incident_report_original.csv";
	public static String OUTPUT_CSV_PATH 	= "res/crime_incident_report_output.csv";
	public static String WEATHER_BY_DAY_PATH = "res/weather_data_byday.txt";
	
	public static String NOCRIME_LABEL 	= "NONE";
	public static String NULL_VALUE 	= "?";
	public static String DELIM 			= ",";
	
	public final static String HEADER = "DAYOFYR" + DELIM +
										"HROFDAY" + DELIM + 
										"RAIN" + DELIM + 
										"SNOW" + DELIM + 
										"TEMP" + DELIM + 
										"POP" + DELIM + 
										"WHITEPER" + DELIM + 
										"BLACKPER" + DELIM + 
										"HISPPER" + DELIM + 
										"ASIANPER" + DELIM + 
										"OTHERPER" + DELIM + 
										"MIXPER" + DELIM + 
										"OCCUPIED_HSNG" + DELIM + 
										"VCNT_HSING" + DELIM + 
										"MDN_INCM" + DELIM + 
										"WEAPON" + DELIM + 
										"CRIMETYPE" + DELIM + 
										"ZONE" + DELIM + 
										"STREET";

	
	public static final boolean write_testing_data 	= false;
	public static final boolean write_nones 		= false;

	/**
	 * Main method
	 * @param args
	 */
	public static void main(String[] args) 
	{
		BufferedWriter writer = null;
		BufferedReader reader = null;

		// This is used for testing & debugging purpose. 
		// Used to produce the 2013 data
		if (write_testing_data)
			System.out.println("Writing testing data.");
		else
			System.out.println("Writing training data.");

		int lines_read 		= 0;
		int lines_written 	= 0;

		HashMap<String,String> temp_map = loadTemperatures();
		HashSet<String> missingTempRecordsSet 			= new HashSet<String>();

		try 
		{
			writer = new BufferedWriter(new FileWriter(new File(OUTPUT_CSV_PATH)));
			reader = new BufferedReader(new FileReader(new File(ORIGINAL_CSV_PATH)));

			String line = null;

			//skip input header
			line 		= reader.readLine();

			//writing the output header
			writer.write(HEADER);
			writer.newLine();

			String[] split 	= null;
			String fromDate = null;
			
			// Variables for all the columns present in the CSV file
			String weaponType 		= null;
			//String buildingType 	= null;
			//String victimActivity 	= null;
			//String unusualActions 	= null;
			String weather 			= null;
			String neighborhood 	= null;
			//String lighting 		= null;
			//String robberyType 		= null;
			//String robberyAttempt 	= null;
			String domestic 		= null;  
			String zone 			= null;                  
			String crimeType 		= null;   
			String streetName 		= null;
			//String crossStreet 		= null;
			
			String[] dateData 		= null;
			String[] weatherData 	= null;
			String[] zoneData 		= null; 	
			String zone_output 		= null;
			String street_output	= null;
			String weaponType_output = null;
			String crimeType_output	= null; 
			
			String toWrite 			= null;
			
			//Parsing all the records present in the CSV file..
			while ((line = reader.readLine()) != null) 
			{
				lines_read++; //counter for the lines read

				// Default value for the records not having Street info
				if(line.charAt(line.length()-1) == ',')
				{
					line += NULL_VALUE;
				}

				split = line.split(",");
				//skipping invalid lines
				if(split.length != 15) 
				{
					System.err.println("Invalid line ( length " + split.length + ")  " +  line);
					continue;
				}

				fromDate 		= split[0];
				// Testing purpose only
				if(write_testing_data && !fromDate.contains("2013"))
					continue;

				weaponType 		= split[1];
				//buildingType 	= split[2];	//not used
				//victimActivity 	= split[3]; //not used
				//unusualActions 	= split[4]; //not used
				weather 		= split[5];
				neighborhood 	= split[6]; //not used
				//lighting 		= split[7]; //not used
				//robberyType 	= split[8]; //not used
				//robberyAttempt 	= split[9]; //not used
				domestic 		= split[10];  
				zone 			= split[11];                  
				crimeType 		= split[12];   
				streetName 		= split[13].toLowerCase();	//not used
				//crossStreet 	= split[14];     			//not used

				//skipping the Domestic crimes..
				if(domestic.toLowerCase().contains("yes"))
					continue;

				// Date Info
				dateData 	= CSVGen.retrieveTemperature(fromDate, temp_map, missingTempRecordsSet);
				
				// Weather Info
				weatherData = CSVGen.parseWeatherType(weather).split(DELIM);

				// Zone Data
				zoneData 	= parseZones(zone).split(DELIM);
				zone_output = zone.replaceAll("[^A-Za-z0-9]", "");
				// Records with incomplete Zone info are skipped
				if(zone.length() < 2)
				{
					System.out.println(lines_read + " ---   " + neighborhood);
					continue;
				}
				
				// Street Info
				street_output = streetName.replaceAll(" ", "_");
				// Records with incomplete Street info are skipped
				if (street_output.length() < 2){
					continue;
				}
				
				//Weapon Type
				weaponType_output 	= CSVGen.parseWeaponType(weaponType);
				
				//Crime Type
				crimeType_output 	= CSVGen.parseCrimeType(crimeType);
				// Records with invalid crime types are ignored.
				if(crimeType_output == null) 
					continue;

				//Info to be written to the file
				toWrite = dateData[0] + DELIM + 	// Day of the Year 
						dateData[1] + 	DELIM + 	// Hour of the Day
						weatherData[0] +DELIM + 	// Raining Info 
						weatherData[1] +DELIM +		// Snowing Info
						dateData[2] + 	DELIM + 	// Mean Temperature of the day 
						zoneData[0] + 	DELIM +		// Total Population of Zone 
						zoneData[1] + 	DELIM + 	// While Population in the Zone
						zoneData[2] + 	DELIM +		// Black Population in the Zone 
						zoneData[3] + 	DELIM + 	// Hispanic Population in the Zone
						zoneData[4] + 	DELIM +		// Asian Population in the Zone 
						zoneData[5] + 	DELIM +		// Other Population in the Zone 
						zoneData[6] + 	DELIM +		// Mixed Population in the Zone
						zoneData[7] + 	DELIM +		// Occupied Housing in the Zone 
						zoneData[8] + 	DELIM + 	// Vacant Housing in the Zone
						zoneData[9] + 	DELIM +		// Median Income in the Zone 
						weaponType_output 	+ 	DELIM + // Weapon Type
						crimeType_output 	+ 	DELIM + // Crime Type
						zone_output 		+ 	DELIM + // Zone
						street_output;					// Street


				//Writing the info to the file.
				writer.write(toWrite);
				writer.newLine();
				lines_written++;

				// Testing purpose only
				if(!write_testing_data && write_nones)
					writeFakeEntry(toWrite,writer,lines_written);
			}

			System.out.println("Finished.");
			System.out.println(lines_read + " lines read.");
			System.out.println(lines_written + " lines written. (" + (lines_read - lines_written) + " records skipped.)");
			System.out.println("missing temperature values:" + missingTempRecordsSet.size());

			if (missingTempRecordsSet.size() > 0)
			{
				System.out.println("Missing day list:");
				for(String str : missingTempRecordsSet)
				{
					System.out.println(str);
				}
			}

		}
		catch (Exception e) 
		{

			e.printStackTrace();
		}
		finally
		{
			try 
			{
				reader.close();
				writer.close();
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Retrieves the temperature for a day of the year
	 * Returns an array of the Day of the Year, Hour of the Day and
	 * the Temperature of the Day
	 * @param fromDate
	 * @param temperatures_map
	 * @param no_temp_set
	 * @return
	 */
	public static String[] retrieveTemperature(String fromDate, 
			HashMap<String,String> temperatures_map, HashSet<String> no_temp_set)
	{
		String[] dateData = CSVGen.parseFromDate(fromDate).split(DELIM);
		String dayOfYear_output = dateData[0];
		String hourOfDay_output = dateData[1];

		int index 	= fromDate.lastIndexOf("/");
		String year = fromDate.substring(index+1,index+5);
		
		String temp_output = "?";

		if(temperatures_map.containsKey(year + DELIM + dayOfYear_output))
		{
			temp_output = temperatures_map.get(year + DELIM + dayOfYear_output);
		}
		else
		{  // if we dont have a temperature
			no_temp_set.add(fromDate.substring(0,fromDate.lastIndexOf(" ")));
		}
		return (new String[] {dayOfYear_output, hourOfDay_output, temp_output});
	}

	public static void writeFakeEntry(String input, BufferedWriter writer, int lines_written) throws IOException
	{
		/*
		 *                dayOfYear_output + DELIM + hourOfDay_output + DELIM + raining_output + DELIM + snowing_output + DELIM
						+ temp_output + DELIM + pop_output + DELIM + white_per_output + DELIM + black_per_output + DELIM 
						+ hisp_per_output + DELIM + asian_per_output + DELIM + other_per_output + DELIM + mix_per_output + DELIM
						+ occupied_housing_output + DELIM + vacant_housing_output + DELIM + median_income_output + DELIM 
						+ weaponType_output + DELIM + crimeType_output + DELIM + zone_output + DELIM + street_output;


		 */
		String[] data = input.split(DELIM);
		int hour = Integer.parseInt(data[1]);
		String crimeType = data[16];


		for(int i = 0; i < 24; i++)
		{
			if(i == hour)
				continue;
			String output = input;

			int firstCommaIndex = output.indexOf(DELIM);
			output = output.substring(firstCommaIndex+1);

			int secondCommaIndex = output.indexOf(DELIM);
			output = output.substring(secondCommaIndex+1);

			output = data[0] + DELIM + Integer.toString(i) + DELIM +
					output.replace(crimeType, NOCRIME_LABEL);
			writer.write(output);
			writer.newLine();
			lines_written++;
		}
	}

	/**
	 * Parses the zone field of the CSV file and returns the 
	 * zone under which we want to classify it under. 
	 * Various values:
	 * allston, roxbury, dorchester, charlestown, south boston, east boston,
	 * fenway, jamaica, mattapan, mission hill, north end, roslindale,
	 * south end, bay village, back bay, downtown, brighton, south zone
	 * @param zone
	 * @return String
	 */
	public static String parseZones(String zone)
	{
		zone = zone.toLowerCase();

		if(zone.contains("allston"))
			return DemographicData.allston_dem;
		if(zone.contains("roxbury"))
			return DemographicData.roxbury_missionhill_longwood_dem;
		if(zone.contains("dorchester"))
			return DemographicData.dorchester_dem;
		if(zone.contains("charlestown"))
			return DemographicData.charlestown_dem;
		if(zone.contains("south boston"))
			return DemographicData.south_boston_dem;
		if(zone.contains("east boston"))
			return DemographicData.east_boston_dem;
		if(zone.contains("fenway"))
			return DemographicData.fenway_kenmore_dem;
		if(zone.contains("jamaica"))
			return DemographicData.jamaicanplain_dem;
		if(zone.contains("mattapan"))
			return DemographicData.mattapan_dem;
		if(zone.contains("mission hill"))
			return DemographicData.roxbury_missionhill_longwood_dem;
		if(zone.contains("north end"))
			return DemographicData.north_end_dem;
		if(zone.contains("roslindale"))
			return DemographicData.rosindale_dem;
		if(zone.contains("south end"))
			return DemographicData.southend_bayvillage_dem;
		if(zone.contains("bay village"))
			return DemographicData.beaconhill_westend_dem;
		if(zone.contains("back bay"))
			return DemographicData.backbay_dem;
		if(zone.contains("downtown"))
			return DemographicData.downtown_chinatown_leatherdist_dem;
		if(zone.contains("brighton"))
			return DemographicData.brighton_dem;
		if(zone.contains("south zone"))
			return DemographicData.hyde_park_dem;
		
		return NULL_VALUE;
	}

	/**
	 * Parses the Weather data present in WEATHER_BY_DAY_PATH file
	 * and creates a Map with the Year and the Day of the Year as the key
	 * and the Temperature as the value
	 * A normal line would look like:    20100201 , 33.9
	 * @return HashMap
	 */
	public static HashMap<String,String> loadTemperatures()
	{
		SimpleDateFormat sd 	= new SimpleDateFormat("yyyyMMdd");
		BufferedReader reader 	= null;
		String line 		= null;
		String dayOfYear 	= null;
		HashMap<String,String> temp_map = new HashMap<String,String>();

		try 
		{
			reader = new BufferedReader(new FileReader(new File(WEATHER_BY_DAY_PATH)));

			//skip the first line as it is the header
			reader.readLine();
			
			String[] split 	= null;
			String year 	= null;
			Date currentDate 		= null;
			GregorianCalendar cal 	= null;
			while ((line = reader.readLine()) != null) 
			{
				split 		= line.split(DELIM);
				currentDate = sd.parse(split[0].trim());
				cal 		= new GregorianCalendar();
				cal.setTime(currentDate);
				dayOfYear 	= Integer.toString(cal.get(GregorianCalendar.DAY_OF_YEAR));
				year = line.substring(0,4);
				
				temp_map.put(year + DELIM + dayOfYear, split[1].trim());
			}
		}
		catch (Exception e) 
		{
			System.err.println("ERROR READING WEATHER FILE " + e.getMessage());
		}
		finally
		{
			try 
			{
				reader.close();
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		}

		System.out.println("Imported " + temp_map.size() + " temperatures into hashmap.");
		return temp_map;
	}

	/**
	 * Parses the crime type field of the CSV file and returns the 
	 * crimeType under which we want to classify it under. 
	 * Various types:
	 * murder, auto_theft, robbery, assault, breaking, stolen_property,
	 * disorderly_people, vandalism, others
	 * @param crimeType
	 * @return
	 */
	public static String parseCrimeType(String crimeType)
	{
		// crime types considered
		// robbery/theft, assault & battery, breaking & entering (b&e), larceny

		String result = "";
		crimeType = crimeType.toLowerCase();

		if(crimeType.equals("") || crimeType.contains("n/a"))
		{
			result = NULL_VALUE;
		}
		else if(crimeType.contains("murder"))
		{
			result = "murder";
		}
		else if(crimeType.contains("auto theft") || crimeType.contains("stolen"))
		{
			result = "auto_theft";
		}
		else if(crimeType.contains("robb"))
		{
			result = "robbery";
		}
		else if(crimeType.contains("assault") 
				|| crimeType.contains("battery") 
				|| crimeType.contains("a&b"))
		{
			result = "assault";
		}
		else if(crimeType.contains("b&e"))
		{
			result = "breaking";
		}

		else if(crimeType.contains("larc"))
		{
			result = "stolen_property";
		}
		else if(crimeType.contains("disorderly person") 
				|| crimeType.contains("disturbing the peace") 
				|| crimeType.contains("affray"))
		{
			result = "disorderly_people";
		}
		else if(crimeType.contains("vandalism"))
		{
			result = "vandalism";
		}
		else
		{
			result = null;
		}
		return result;
	}

	/**
	 * Parses the weather field of the CSV file and returns the 
	 * weatherType under which we want to classify it under. 
	 * Various types:
	 * 1) snow / sleet / blizzard
	 * 2) rain / drizz
	 * @param weather
	 * @return String
	 */
	public static String parseWeatherType(String weather)
	{
		weather = weather.toLowerCase();

		boolean raining = false;
		boolean snowing = false;

		// Setting the boolean raining
		if(weather.contains("rain") || weather.contains("drizz") )
		{
			raining = true;
		}

		// Setting the boolean snowing
		if(weather.contains("snow") || weather.contains("sleet") || weather.contains("blizz"))
		{
			snowing = true;
		}
		return raining + DELIM + snowing;
	}

	/**
	 * Parses the weapon field of the CSV file and returns the 
	 * weaponType under which we want to classify it under. 
	 * @param weaponType
	 * @return String
	 */
	public static String parseWeaponType(String weaponType)
	{
		weaponType = weaponType.toLowerCase();
		String result = null;

		// Blank fields or No weapons used in the Crime
		if(weaponType.equals("") || weaponType.contains("n/a"))
		{
			result = NULL_VALUE;
		}
		//Gun or pistol used as the weapon
		else if(weaponType.contains("gun") || weaponType.contains("pistol"))
		{
			result = "gun";
		}
		//Knife used as a weapon
		else if(weaponType.contains("knife") || weaponType.contains("knives"))
		{
			result = "knife";
		}
		//other weapon
		else
		{
			result = "other";
		}

		return result;
	}

	/**
	 * Retrieves the Day of the year and the Hour of the Day from the
	 * given fromDate; "DAYOFYEAR,HOUROFDAY" string
	 * @param fromDate
	 * @returns String 
	 */
	public static String parseFromDate(String fromDate)
	{
		String[] dateSplit 	= fromDate.split(" ");
		SimpleDateFormat sd = new SimpleDateFormat("MM/dd/yyyy");
		String dayOfYear 	= null;

		try {
			Date currentDate 		= sd.parse(dateSplit[0]);
			GregorianCalendar cal 	= new GregorianCalendar();
			cal.setTime(currentDate);

			dayOfYear 	= Integer.toString(cal.get(GregorianCalendar.DAY_OF_YEAR));
		} catch (ParseException e) {
			System.err.println("Error parsing date:" + fromDate);
			return null;
		}

		String hourOfDay = dateSplit[1].substring(0,2);
		if (hourOfDay == null)
			hourOfDay = "";
		else
			hourOfDay = hourOfDay.replaceAll("[^\\d.]", "");

		return dayOfYear + DELIM + hourOfDay;
	}

}
